import matplotlib.pyplot as plt
import numpy as np

h_thin_0 = 300
h_thick_0 = 75
H_thin_0 = 3.5e3
H_thick_0 = 4.3e3

rho_thin_0 = 0.78
rho_thick_0 = 0.02

R_bulge = 1.5e3
R_halo = 5.0e3

rho_bulge_0 = 1.5
rho_halo_0 = 0.005

x_bulge = 4.
x_halo = 3.

rho_thin = np.zeros(1001)
rho_thick = np.zeros(1001)
rho_bulge = np.zeros(1001)
rho_halo = np.zeros(1001)
rho_total = np.zeros(1001)

R = np.arange(0, 1e5+100, 100)
for i in range(1001):
    rho_thin[i] = rho_thin_0*np.exp(-R[i]/H_thin_0)
    rho_thick[i] = rho_thick_0*np.exp(-R[i]/H_thick_0)
    rho_bulge[i] = rho_bulge_0*(1 + (R[i]/R_bulge))**-x_bulge
    rho_halo[i] = rho_halo_0*(1 + (R[i]/R_halo))**-x_halo

rho_total = rho_thin + rho_thick + rho_bulge + rho_halo
thin_frac_0 = rho_thin[0]/rho_total[0]
thick_frac_0 = rho_thick[0]/rho_total[0]
bulge_frac_0 = rho_bulge[0]/rho_total[0]
halo_frac_0 = rho_halo[0]/rho_total[0]
thin_frac_85 = rho_thin[85]/rho_total[85]
thick_frac_85 = rho_thick[85]/rho_total[85]
bulge_frac_85 = rho_bulge[85]/rho_total[85]
halo_frac_85 = rho_halo[85]/rho_total[85]
thin_frac_100 = rho_thin[1000]/rho_total[1000]
thick_frac_100 = rho_thick[1000]/rho_total[1000]
bulge_frac_100 = rho_bulge[1000]/rho_total[1000]
halo_frac_100 = rho_halo[1000]/rho_total[1000]

print 'R=0'
print 'thin disk', thin_frac_0
print 'thick disk', thick_frac_0
print 'bulge', bulge_frac_0
print 'halo', halo_frac_0

print 'R=8500pc'
print 'thin disk', thin_frac_85
print 'thick disk', thick_frac_85
print 'bulge', bulge_frac_85
print 'halo', halo_frac_85

print 'R=100kpc'
print 'thin disk', thin_frac_100
print 'thick disk', thick_frac_100
print 'bulge', bulge_frac_100
print 'halo', halo_frac_100

one, = plt.plot(R, (rho_thin), ls = '--', label='thin disk')

two, = plt.plot(R, (rho_thick), ls = '-', label='thick disk')

three, = plt.plot(R, (rho_bulge), ls = '-.', label='bulge')

four, = plt.plot(R, (rho_halo), ls = ':', label='halo')
plt.title("Galactic Density Plot")
plt.legend(handles=[one, two, three, four])
plt.show()
