from __future__ import division
from matplotlib import pyplot as plt
import numpy as np
import scipy as sp

pc_to_meters = 3.086e18
rho_0 = 0.0035 #solar masses per parsec^3
r_0 = 5000 #pc
r_max = 50000 #pc
r_max *= pc_to_meters #centimeters
r_init = 6000 #pc
r_init *= pc_to_meters
psi_init = 0
v_psi_init = 100 #km/s
v_psi_init *= 100000 #cm/s
v_r_init = -43.5 #km/s
v_r_init *= 100000 #cm/s

def potential(r):
    pc_to_meters = 3.086e18
    rho_0 = 0.0035 #solar masses per parsec^3
    rho_0 *= (1/((3.086e18)**3))
    rho_0 *= 1.989e33 #gs per m^3
    r_0 = 5000 #pc
    r_0 *= pc_to_meters #meters
    r_max = 50000 #pc
    r_max *= pc_to_meters # meters
    G = 6.67*(10**-8) # cm^3/g/s^2

    phi = -4*np.pi*G*rho_0*(r_0**2)*(1 + np.log(r_max/r))
    return phi
print np.log(r_max/r_init)


L = v_psi_init*r_init

E = (potential(r_init) + (0.5)*((v_r_init)**2) + (0.5)*((v_psi_init)**2))
print potential(r_init)
print E
print v_r_init**2

v_r = v_r_init
v_psi = v_psi_init
r = r_init 
psi = psi_init
t = 0

dt = 5*(10**5)*(365*24*60*60)
t_max = dt*80000


time = np.zeros(t_max/dt)
radius = np.zeros(t_max/dt)
angle = np.zeros(t_max/dt)
x = np.zeros(t_max/dt)
y = np.zeros(t_max/dt)

i = 0
sign = -1
v_r_1 = 0
thing = True
while (t < t_max):
    #store parameters
    time[i] = t
    radius[i] = r
    angle[i] = psi 
    x[i] = r*np.cos(psi)
    y[i] = r*np.sin(psi)

    #Enforce conservation
    r_1 = r + v_r*dt
    v_psi_1 = L/r_1
    psi_1 = psi + (v_psi_1/r_1)*dt
    if ( 2*(E - potential(r_1) - (0.5)*((v_psi_1)**2)) < 0):
        sign *= -1
    else:
        sign *= 1
    v_r_1 = sign*np.sqrt(np.abs(2*(E - potential(r_1) - (0.5)*((v_psi_1)**2))))
    #print v_r_1

    if ((psi > 2*np.pi) & (thing==True)):
        print t
        thing=False
    #update parameters
    r = r_1
    psi = psi_1
    v_r = v_r_1
    v_psi = v_psi_1
    
    t += dt
    i += 1
peri_galacticon = time[np.where(radius < (min(radius) + 5e17))]
print peri_galacticon[1] - peri_galacticon[0]
x /= pc_to_meters
y /= pc_to_meters
plt.plot(x, y,)
plt.title('Altered Orbit Trajectory')
plt.xlabel('X-axis (pc)')
plt.ylabel('Y-axis (pc)')
#print x
#print y
#print radius
#print angle
plt.show()
#print max(radius)
