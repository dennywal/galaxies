# README #

This is a collection of a few homework assignments I completed for a Galactic astronomy course. 

Homework 6 contains a plot of the density of a galaxy in the bulge, halo and disk.

Homework 10 contains a numerically simulated trajectory of a star in a galaxy with a specified mass distribution.